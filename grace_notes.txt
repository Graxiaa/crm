TUTORIAL LINK: https://www.youtube.com/playlist?list=PL-51WBLyFTg2vW-_6XBoUpE7vpmoR3ztO

1. Install django
	pip install django

2.Create Project
	django-admin startproject <name>

3. Create App
	python manage.py startapp <name>


DJANGO ADMIN
1. Migrate admin 
	python manage.py migrate

2.Create Super User
	python manage.py createsuperuser



COMMANDS
- Create Migrations
	python manage.py makemigrations
- Run Migrations
	python manage.py migrate



DOCUMENTATIONS
- QuerySet API Reference
	https://docs.djangoproject.com/en/3.0/ref/models/querysets/




LIBRARIES
- Install Filter
    pip install django-filter

- Install Pillow (Image upload)
    pip install Pillow

- Install Postgres Connection
    pip install psycopg2
